INTRODUCTION
------------

My scripts for AutoHotKey


REQUIREMENTS
------------

* AutoHotKey (http://ahkscript.org/)
* VirtualDesktopAccessor (https://github.com/Ciantic/VirtualDesktopAccessor)


INSTALLATION
------------

    <InstallDIr>/
      icons/
        1.ico
        2.ico
        3.ico
         ...
      AutoHotkey.ahk
      AutoHotkey.exe
      VDA.ahk
      VirtualDesktopAccessor.dll
