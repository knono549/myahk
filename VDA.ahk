;;
;; Copyright (c) 2016-2023 knono549
;; This software is released under the MIT License.
;; http://opensource.org/licenses/mit-license.php
;;

;; @return void
VDA_init() {
    global VDA_g_GetCurrentDesktopNumberProc
         , VDA_g_GetDesktopCountProc
         , VDA_g_IsWindowOnCurrentVirtualDesktopProc
         , VDA_g_MoveWindowToDesktopNumberProc
         , VDA_g_GoToDesktopNumberProc
         , VDA_g_RegisterPostMessageHookProc
         , VDA_g_UnregisterPostMessageHookProc
         , VDA_g_IsPinnedWindowProc
         , VDA_g_PinWindowProc
         , VDA_g_UnPinWindowProc
         , VDA_g_ahkWinId
         , VDA_g_msgOffset
         , VDA_g_lastActiveWins
         , VDA_g_listeners

    hVDA := DllCall("LoadLibrary", "Str", "VirtualDesktopAccessor.dll", "Ptr")
    VDA_g_GetCurrentDesktopNumberProc
      := DllCall("GetProcAddress", "Ptr", hVDA
               , "AStr", "GetCurrentDesktopNumber", "Ptr")
    VDA_g_GetDesktopCountProc
      := DllCall("GetProcAddress", "Ptr", hVDA
               , "AStr", "GetDesktopCount", "Ptr")
    VDA_g_IsWindowOnCurrentVirtualDesktopProc
      := DllCall("GetProcAddress", "Ptr", hVDA
               , "AStr", "IsWindowOnCurrentVirtualDesktop", "Ptr")
    VDA_g_MoveWindowToDesktopNumberProc
      := DllCall("GetProcAddress", "Ptr", hVDA
               , "AStr", "MoveWindowToDesktopNumber", "Ptr")
    VDA_g_GoToDesktopNumberProc
      := DllCall("GetProcAddress", "Ptr", hVDA
               , "AStr", "GoToDesktopNumber", "Ptr")
    VDA_g_RegisterPostMessageHookProc
      := DllCall("GetProcAddress", "Ptr", hVDA
               , "AStr", "RegisterPostMessageHook", "Ptr")
    VDA_g_UnregisterPostMessageHookProc
      := DllCall("GetProcAddress", "Ptr", hVDA
               , "AStr", "UnregisterPostMessageHook", "Ptr")
    VDA_g_IsPinnedWindowProc
      := DllCall("GetProcAddress", "Ptr", hVDA
               , "AStr", "IsPinnedWindow", "Ptr")
    VDA_g_PinWindowProc
      := DllCall("GetProcAddress", "Ptr", hVDA, "AStr", "PinWindow", "Ptr")
    VDA_g_UnPinWindowProc
      := DllCall("GetProcAddress", "Ptr", hVDA, "AStr", "UnPinWindow", "Ptr")

    VDA_g_ahkWinId
      := WinExist("ahk_pid" . DllCall("GetCurrentProcessId", "UInt"))
    VDA_g_msgOffset := 0x1400 + 30
    VDA_g_lastActiveWins := Map()
    VDA_g_listeners := []

    VDA_registerPostMessageHook(VDA_g_ahkWinId + (0x1000 << 32), VDA_g_msgOffset)
    OnMessage(VDA_g_msgOffset, VDA_onEventBridge)
    OnMessage(VDA_g_msgOffset + 1, VDA_onEventBridge)
    OnMessage(VDA_g_msgOffset + 2, VDA_onEventBridge)
    OnMessage(VDA_g_msgOffset + 3, VDA_onEventBridge)
    OnMessage(VDA_g_msgOffset + 4, VDA_onEventBridge)
    OnMessage(VDA_g_msgOffset + 5, VDA_onEventBridge)

    VDA_addListener(VDA_onEvent)
}

;; @return void
VDA_deinit() {
    VDA_removeListener(VDA_onEvent)

    OnMessage(VDA_g_msgOffset, VDA_onEventBridge, 0)
    OnMessage(VDA_g_msgOffset + 1, VDA_onEventBridge, 0)
    OnMessage(VDA_g_msgOffset + 2, VDA_onEventBridge, 0)
    OnMessage(VDA_g_msgOffset + 3, VDA_onEventBridge, 0)
    OnMessage(VDA_g_msgOffset + 4, VDA_onEventBridge, 0)
    OnMessage(VDA_g_msgOffset + 5, VDA_onEventBridge, 0)
    VDA_unregisterPostMessageHook(VDA_g_ahkWinId + (0x1000 << 32))
}

;; @return -1 (failed), 0..n (succeeded)
VDA_getCurrentDesktopNumber() {
    global VDA_g_GetCurrentDesktopNumberProc
    Return DllCall(VDA_g_GetCurrentDesktopNumberProc, "Int")
}

;; @return -1 (failed), 1..n (succeeded)
VDA_getDesktopCount() {
    global VDA_g_GetDesktopCountProc
    Return DllCall(VDA_g_GetDesktopCountProc, "Int")
}

;; @param aWinId a window id
;; @return -1 (failed), 0 (false) or 1 (true)
VDA_isWindowOnCurrentVirtualDesktop(aWinId) {
    global VDA_g_IsWindowOnCurrentVirtualDesktopProc
    Return DllCall(VDA_g_IsWindowOnCurrentVirtualDesktopProc
                 , "UInt", aWinId, "UInt")
}

;; @param aWinId a window id
;; @param aNum a desktop number
;; @return void
VDA_moveWindowToDesktopNumber(aWinId, aNum) {
    global VDA_g_MoveWindowToDesktopNumberProc

    num := VDA_getCurrentDesktopNumber() ;may be -1
    if (aNum != num) {
        VDA_setLastActiveWin(aNum, 0)
        winId := WinGetID("A")
        if (aWinId = winId) {
            Try {
                WinActivate("ahk_class Shell_TrayWnd") ;avoid switching desktop
            } Catch TargetError as e {
            }
        }
    }

    Return DllCall(VDA_g_MoveWindowToDesktopNumberProc
                 , "UInt", aWinId, "Int", aNum, "Int")
}

;; @param aNum a desktop number
;; @return void
VDA_goToDesktopNumber(aNum) {
    global VDA_g_GoToDesktopNumberProc

    num := VDA_getCurrentDesktopNumber() ;may be -1
    if (aNum != num) {
        winId := WinGetID("A")
        pinned := VDA_isPinnedWindow(winId)
        onCur := VDA_isWindowOnCurrentVirtualDesktop(winId)
        if (num != -1) {
            VDA_setLastActiveWin(num, (pinned = 0 && onCur = 1) ? winId : 0)
        }
        if (pinned != 1) {
            Try {
                WinActivate("ahk_class Shell_TrayWnd")
            } Catch TargetError as e {
            }
        }
    }

    DllCall(VDA_g_GoToDesktopNumberProc, "Int", aNum)
}

;; @param aWinId a window id
;; @param aMsgOffset a message offset
;; @return void
VDA_registerPostMessageHook(aWinId, aMsgOffset) {
    global VDA_g_RegisterPostMessageHookProc
    DllCall(VDA_g_RegisterPostMessageHookProc
          , "UInt", aWinId, "Int", aMsgOffset)
}

;; @param aWinId a window id
;; @return void
VDA_unregisterPostMessageHook(aWinId) {
    global VDA_g_UnregisterPostMessageHookProc
    DllCall(VDA_g_UnregisterPostMessageHookProc, "UInt", aWinId)
}

;; @param aFunc a function object
;; @return 0 (not found) or 1..n (found)
VDA_findListener(aFunc) {
    global VDA_g_listeners
    index := 0
    cnt := VDA_g_listeners.Length
    Loop (cnt) {
        if (aFunc = VDA_g_listeners[A_Index]) {
            index := A_Index
            Break
        }
    }
    Return index
}

;; @param aFunc a function object
;; @return void
VDA_addListener(aFunc) {
    global VDA_g_listeners
    index := VDA_findListener(aFunc)
    if (index = 0) {
        VDA_g_listeners.Push(aFunc)
    }
}

;; @param aFuncName a function object
;; @return void
VDA_removeListener(aFunc) {
    global VDA_g_listeners
    index := VDA_findListener(aFunc)
    if (index != 0) {
        VDA_g_listeners.RemoveAt(index)
    }
}

;; @param aWParam
;;   VDA_CurrentVirtualDesktopChanged: the old desktop number 
;;   VDA_ViewVirtualDesktopChanged   : 0
;;   VDA_VirtualDesktopDestroyed     : the destroyed desktop number
;;   VDA_VirtualDesktopDestroyFailed : the destroyed desktop number
;;   VDA_VirtualDesktopDestroyBegin  : the destroyed desktop number
;;   VDA_VirtualDesktopCreated       : the created desktop number
;; @param aLParam
;;   VDA_CurrentVirtualDesktopChanged: the new desktop number
;;   VDA_ViewVirtualDesktopChanged   : 0
;;   VDA_VirtualDesktopDestroyed     : the fallback desktop number
;;   VDA_VirtualDesktopDestroyFailed : the fallback desktop number
;;   VDA_VirtualDesktopDestroyBegin  : the fallback desktop number
;;   VDA_VirtualDesktopCreated       : 0
;; @param aMsg
;;   VDA_CurrentVirtualDesktopChanged: 0 + offset
;;   VDA_ViewVirtualDesktopChanged   : 1 + offset
;;   VDA_VirtualDesktopDestroyed     : 2 + offset
;;   VDA_VirtualDesktopDestroyFailed : 3 + offset
;;   VDA_VirtualDesktopDestroyBegin  : 4 + offset
;;   VDA_VirtualDesktopCreated       : 5 + offset
;; @param aWinId VDA_g_ahkWinId + offset
;; @return void
VDA_onEventBridge(aWParam, aLParam, aMsg, aWinId) {
    global VDA_g_msgOffset, VDA_g_listeners
    msg := aMsg - VDA_g_msgOffset
    cnt := VDA_g_listeners.Length
    Loop (cnt) {
        listener := VDA_g_listeners[A_Index]
        listener(aWParam, aLParam, msg, aWinId)
    }
}

;; @param aWParam
;;   VDA_CurrentVirtualDesktopChanged: the old desktop number 
;;   VDA_ViewVirtualDesktopChanged   : 0
;;   VDA_VirtualDesktopDestroyed     : the destroyed desktop number
;;   VDA_VirtualDesktopDestroyFailed : the destroyed desktop number
;;   VDA_VirtualDesktopDestroyBegin  : the destroyed desktop number
;;   VDA_VirtualDesktopCreated       : the created desktop number
;; @param aLParam
;;   VDA_CurrentVirtualDesktopChanged: the new desktop number
;;   VDA_ViewVirtualDesktopChanged   : 0
;;   VDA_VirtualDesktopDestroyed     : the fallback desktop number
;;   VDA_VirtualDesktopDestroyFailed : the fallback desktop number
;;   VDA_VirtualDesktopDestroyBegin  : the fallback desktop number
;;   VDA_VirtualDesktopCreated       : 0
;; @param aMsg
;;   VDA_CurrentVirtualDesktopChanged: 0
;;   VDA_ViewVirtualDesktopChanged   : 1
;;   VDA_VirtualDesktopDestroyed     : 2
;;   VDA_VirtualDesktopDestroyFailed : 3
;;   VDA_VirtualDesktopDestroyBegin  : 4
;;   VDA_VirtualDesktopCreated       : 5
;; @param aWinId VDA_g_ahkWinId
;; @return void
VDA_onEvent(aWParam, aLParam, aMsg, aWinId) {
    if (aMsg != 0) { ;VDA_CurrentVirtualDesktopChanged
        Return
    }

    winId := WinGetID("A")
    pinned := VDA_isPinnedWindow(winId)
    if (pinned != 1) {
        oWinId := VDA_getLastActiveWin(aLParam)
        oOnCur := VDA_isWindowOnCurrentVirtualDesktop(oWinId)
        if (oOnCur = 1) {
            Try {
                WinActivate(Format("ahk_id {1:d}", oWinId))
            } Catch TargetError as e {
            }
        }
    }
}

;; @param aWinId a window id
;; @return -1 (failed), 0 (false) or 1 (true)
VDA_isPinnedWindow(aWinId) {
    global VDA_g_IsPinnedWindowProc
    Return DllCall(VDA_g_IsPinnedWindowProc, "UInt", aWinId, "Int")
}

;; @param aWinId a window id
;; @return void
VDA_pinWindow(aWinId) {
    global VDA_g_PinWindowProc
    DllCall(VDA_g_PinWindowProc, "UInt", aWinId)
}

;; @param aWinId a window id
;; @return void
VDA_unPinWindow(aWinId) {
    global VDA_g_UnPinWindowProc
    DllCall(VDA_g_UnPinWindowProc, "UInt", aWinId)
}

;; @param aNum a desktop number
;; @return the window id
VDA_getLastActiveWin(aNum) {
    global VDA_g_lastActiveWins
    Return VDA_g_lastActiveWins.Get(aNum + 1, 0)
}

;; @param aNum a desktop number
;; @param aWinId a window id
;; @return void
VDA_setLastActiveWin(aNum, aWinId) {
    global VDA_g_lastActiveWins
    VDA_g_lastActiveWins[aNum + 1] := aWinId
}

;; @return void
VDA_goToNextDesktop() {
    cnt := VDA_getDesktopCount()
    num := VDA_getCurrentDesktopNumber()
    If (cnt = -1 || num = -1) {
        Return
    }
    num := (num < cnt - 1) ? num + 1 : 0
    VDA_goToDesktopNumber(num)
}

;; @return void
VDA_goToPrevDesktop() {
    cnt := VDA_getDesktopCount()
    num := VDA_getCurrentDesktopNumber()
    If (cnt = -1 || num = -1) {
        Return
    }
    num := (num > 0) ? num - 1 : cnt - 1
    VDA_goToDesktopNumber(num)
}
