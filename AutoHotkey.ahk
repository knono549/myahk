;;
;; Copyright (c) 2016-2023 knono549
;; This software is released under the MIT License.
;; http://opensource.org/licenses/mit-license.php
;;

;; options
#SingleInstance FORCE
#WinActivateForce
A_MaxHotkeysPerInterval := 80	;default: 70
ProcessSetPriority("High")
SendMode("Input")
DetectHiddenWindows(True)	;for VirtualDesktopAccessor.dll

;; load libs
#include %A_ScriptDir%\VDA.ahk


;; initialize and finalize
AutoExecSub:
K5_init()
Return


;; @return void
K5_init() {
    OnExit(K5_onExit)

    VDA_init()
    VDA_addListener(K5_onVDAEvent)
    K5_updateTrayIcon(VDA_getCurrentDesktopNumber())
}

;; @return void
K5_deinit() {
    VDA_removeListener(K5_onVDAEvent)
    VDA_deinit()
}

;; @param aReason an exit reason
;; @param aCode a exit code
;; @return void
K5_onExit(aReason, aCode) {
    K5_deinit()
    ExitApp()
}

;; rebuild custom window menu
;; @param aWinId a window id
;; @return void
K5_rebuildCustomWinMenu(aWinId) {
    global K5_g_customWinMenu

    if (!IsSet(K5_g_customWinMenu)) {
        K5_g_customWinMenu := Menu()
    }

    K5_g_customWinMenu.Delete()

    flag := 0 ;to exclude taskbar, desktop, etc.
    es := WinGetExStyle(Format("ahk_id {1:d}", aWinId))
    If (es & 0x00000080) { ;WS_EX_TOOLWINDOW
        s := WinGetStyle(Format("ahk_id {1:d}", aWinId))
        If ((s & 0x80000000) ;WS_POPUP
         || DllCall("GetWindow"
                  , "UInt", aWinId, "UInt", 4, "UInt") != 0) { ;GW_OWNER
            flag := 1
        }
    }

    K5_g_customWinMenu.Add("Always on &Top", K5_onAlwaysOnTopCmd)
    If (flag = 0) {
        K5_g_customWinMenu.Add("&Always Show", K5_onAlwaysShowCmd)
        K5_g_customWinMenu.Add()
        cnt := VDA_getDesktopCount()
        Loop (cnt) {
            K5_g_customWinMenu.Add(Format("Move to Desktop &{1:d}", A_Index)
                                 , K5_onMoveToDesktopCmd)
        }
    }

    If (es & 0x00000008) { ;WS_EX_TOPMOST
        K5_g_customWinMenu.Check("Always on &Top")
    } Else {
        K5_g_customWinMenu.Uncheck("Always on &Top")
    }

    If (flag = 0) {
        pinned := VDA_isPinnedWindow(aWinId)
        If (pinned = 1) {
            K5_g_customWinMenu.Check("&Always Show")
        } Else If (pinned = 0) {
            K5_g_customWinMenu.Uncheck("&Always Show")
        }

        num := VDA_getCurrentDesktopNumber() + 1
        K5_g_customWinMenu.Disable(Format("Move to Desktop &{1:d}", num))
    }
}

;; @param aItemName a menu item name
;; @param aItemPos a menu item position
;; @pram aMenu a menu object
;; @return void
K5_onAlwaysOnTopCmd(aItemName, aItemPos, aMenu) {
    global K5_g_targetWinId
    K5_toggleAlwaysOnTop(K5_g_targetWinId)
}

;; @param aItemName a menu item name
;; @param aItemPos a menu item position
;; @pram aMenu a menu object
;; @return void
K5_onAlwaysShowCmd(aItemName, aItemPos, aMenu) {
    global K5_g_targetWinId
    K5_toggleAlwaysShow(K5_g_targetWinId)
}

;; @param aItemName a menu item name
;; @param aItemPos a menu item position
;; @pram aMenu a menu object
;; @return void
K5_onMoveToDesktopCmd(aItemName, aItemPos, aMenu) {
    global K5_g_targetWinId
    VDA_moveWindowToDesktopNumber(K5_g_targetWinId
                                , SubStr(aItemName, 18) - 1)
}

;; @param aWinId a window id
;; @return void
K5_toggleAlwaysOnTop(aWinId) {
    ;WinSetAlwaysOnTop(-1, Format("ahk_id {1:d}", aWinId))
    es := WinGetExStyle(Format("ahk_id {1:d}", aWinId))
    ia := (es & 0x00000008) ? -2 : -1 ;WS_EX_TOPMOST, HWND_NOTOPMOST, HWND_TOPMOST
    flags := 0x2000 ;SWP_DEFERERASE
           | 0x0001 ;SWP_NOSIZE
           | 0x0010 ;SWP_NOACTIVATE
           | 0x0400 ;SWP_NOSENDCHANGING
           | 0x0002 ;SWP_NOMOVE
    DllCall("SetWindowPos"
          , "UInt", aWinId, "UInt", ia
          , "Int", 0, "Int", 0
          , "Int", 0, "Int", 0
          , "UInt", flags)
}

;; @param aWinId a window id
;; @return void
K5_toggleAlwaysShow(aWinId) {
    pinned := VDA_isPinnedWindow(aWinId)
    If (pinned = 1) {
        VDA_unPinWindow(aWinId)
    } Else If (pinned = 0) {
        VDA_pinWindow(aWinId)
    }
}

;; @param aWParam
;;   VDA_CurrentVirtualDesktopChanged: the old desktop number 
;;   VDA_ViewVirtualDesktopChanged   : 0
;;   VDA_VirtualDesktopDestroyed     : the destroyed desktop number
;;   VDA_VirtualDesktopDestroyFailed : the destroyed desktop number
;;   VDA_VirtualDesktopDestroyBegin  : the destroyed desktop number
;;   VDA_VirtualDesktopCreated       : the created desktop number
;; @param aLParam
;;   VDA_CurrentVirtualDesktopChanged: the new desktop number
;;   VDA_ViewVirtualDesktopChanged   : 0
;;   VDA_VirtualDesktopDestroyed     : the fallback desktop number
;;   VDA_VirtualDesktopDestroyFailed : the fallback desktop number
;;   VDA_VirtualDesktopDestroyBegin  : the fallback desktop number
;;   VDA_VirtualDesktopCreated       : 0
;; @param aMsg
;;   VDA_CurrentVirtualDesktopChanged: 0
;;   VDA_ViewVirtualDesktopChanged   : 1
;;   VDA_VirtualDesktopDestroyed     : 2
;;   VDA_VirtualDesktopDestroyFailed : 3
;;   VDA_VirtualDesktopDestroyBegin  : 4
;;   VDA_VirtualDesktopCreated       : 5
;; @param aWinId VDA_g_ahkWinId
;; @return void
K5_onVDAEvent(aWParam, aLParam, aMsg, aWinId) {
    if (aMsg != 0) { ;VDA_CurrentVirtualDesktopChanged
        Return
    }

    K5_updateTrayIcon(aLParam)
}

;; @param aNum a desktop number
;; @return void
K5_updateTrayIcon(aNum) {
    num := aNum + 1
    Try {
        TraySetIcon(Format("icons\{1:d}.ico", num))
    } Catch Error as e {
        TraySetIcon("*")
    }
}

;; @param aWinId a window id
;; @param aRect a object that receive values
;; @return 0 (failed), non-0 (succeeded)
K5_getWindowRect(aWinId, &aRect) {
    rect := Buffer(16)
    ret := DllCall("GetWindowRect", "UInt", aWinId, "Ptr", rect.Ptr, "Int")
    if (ret) {
        aRect.left := NumGet(rect.Ptr, 0, "Int")
        aRect.top := NumGet(rect.Ptr, 4, "Int")
        aRect.right := NumGet(rect.Ptr, 8, "Int")
        aRect.bottom := NumGet(rect.Ptr, 12, "Int")
    }
    Return ret
}

;; @return -1 (failed), 0 (false) or 1 (true)
K5_isActiveWindowTrackingEnabled() {
    vparam := Buffer(4)
    ret := DllCall("SystemParametersInfo"
                 , "UInt", 0x1000, "UInt", 0 ;SPI_GETACTIVEWINDOWTRACKING
                 , "Ptr", vparam.Ptr, "UInt", 0
                 , "Int")
    Return ret ? NumGet(vparam.Ptr, 0, "Int") : -1
}

;; @param aFuncName a function object
;; @param aParam a param for aFuncName
;; @return 0 (failed) or non-0 (succeeded)
K5_moveMouseAccordingToActiveWindow(aFunc, aParam) {
    winId := WinGetID("A")

    if (!aFunc(aParam)) {
        Return 0
    }

    if (K5_isActiveWindowTrackingEnabled() != 1) {
        Return 2
    }

    x := 0
    y := 0
    rect := {}
    ; WinGetPos can not get the actual window rect when using Aero Snap
    if (K5_getWindowRect(winId, &rect)) { 
        x := Floor((rect.right - rect.left) / 2)
        y := Floor((rect.bottom - rect.top) / 2)
    }

    MouseGetPos(, , &mWinId)
    if (mWinId != winId) {
        WinActivate(Format("ahk_id {1:d}", winId))
        WinMoveTop(Format("ahk_id {1:d}", winId))
        MouseMove(x, y, 0)
    }

    Return 1
}

;; @param aKeys a key stroke string
;; @return 1 (true)
K5_sendEvent(aKeys) {
    SendEvent(aKeys)
    Return 1
}

;; @return -1 (failed), 0 (false) or n (the window id)
K5_mouseCursorIsInTheTabBarOfFirefox() {
    global K5_g_mousePosWinId

    CoordMode("Mouse", "Screen")
    MouseGetPos(, &y, &winId)

    winClass := WinGetClass(Format("ahk_id {1:d}", winId))
    if (winClass != "MozillaWindowClass") {
        Return 0
    }

    winTitle := WinGetTitle(Format("ahk_id {1:d}", winId))
    suffix := SubStr(winTitle, -7)
    if (suffix != "Firefox" && suffix != "Nightly") {
        Return 0
    }

    rect := {}
    if (!K5_getWindowRect(winId, &rect)) {
        Return 0
    }

    if (y - rect.top > 23) {
        Return 0
    }

    K5_g_mousePosWinId := winId
    Return winId
}

;; @return -1 (failed), 0 (false) or n (the window id)
K5_mouseCursorIsInTheTabBarOfThunderbird() {
    global K5_g_mousePosWinId

    CoordMode("Mouse", "Screen")
    MouseGetPos(, &y, &winId)

    winClass := WinGetClass(Format("ahk_id {1:d}", winId))
    if (winClass != "MozillaWindowClass") {
        Return 0
    }

    winTitle := WinGetTitle(Format("ahk_id {1:d}", winId))
    suffix := SubStr(winTitle, -11)
    if (suffix != "Thunderbird") {
        Return 0
    }

    rect := {}
    if (!K5_getWindowRect(winId, &rect)) {
        Return 0
    }

    if (y - rect.top > 23) {
        Return 0
    }

    K5_g_mousePosWinId := winId
    Return winId
}


;; show custom window menu
!+Space:: {
    global K5_g_targetWinId
    K5_g_targetWinId := WinGetID("A")
    K5_rebuildCustomWinMenu(K5_g_targetWinId)
    K5_g_customWinMenu.Show()
}

;; ignore starting Media Center
;#!Enter:: Return

;; switch virtual desktop
#Tab:: {
    ;PostMessage(1034, 1026, 0, , "VirtuaWinMainClass")
    VDA_goToNextDesktop()
}
#+Tab:: {
    ;PostMessage(1034, 1025, 0, , "VirtuaWinMainClass")
    VDA_goToPrevDesktop()
}
#w:: Send("#{Tab}")	;remap Task View
#n:: Send("#w")		;remap Windows Ink

;; control a window by not using cursor keys;
;; improve compatibility of Active Window Tracking and Aero Snap
#[::
$#Left:: {
    K5_moveMouseAccordingToActiveWindow(K5_sendEvent
                                      , "{LWinDown}{Left}{LWinUp}")
}
#@::
$#Up:: {
    K5_moveMouseAccordingToActiveWindow(K5_sendEvent
                                      , "{LWinDown}{Up}{LWinUp}")
}
#]::
$#Right:: {
    K5_moveMouseAccordingToActiveWindow(K5_sendEvent
                                      , "{LWinDown}{Right}{LWinUp}")
}
#vkba::
$#Down:: {
    K5_moveMouseAccordingToActiveWindow(K5_sendEvent
                                      , "{LWinDown}{Down}{LWinUp}")
}
#{::
$#+Left:: {
    K5_moveMouseAccordingToActiveWindow(K5_sendEvent
                                      , "{LWinDown}+{Left}{LWinUp}")
}
#`::
$#+Up:: {
    K5_moveMouseAccordingToActiveWindow(K5_sendEvent
                                      , "{LWinDown}+{Up}{LWinUp}")
}
#}::
$#+Right:: {
    K5_moveMouseAccordingToActiveWindow(K5_sendEvent
                                      , "{LWinDown}+{Right}{LWinUp}")
}
#*::
$#+Down:: {
    K5_moveMouseAccordingToActiveWindow(K5_sendEvent
                                      , "{LWinDown}+{Down}{LWinUp}")
}

;; move cursor by using non-convert key
vk1d & h:: Send("{Blind}{Left}")
vk1d & j:: Send("{Blind}{Down}")
vk1d & k:: Send("{Blind}{up}")
vk1d & l:: Send("{Blind}{Right}")
vk1d & y:: Send("{Blind}{Home}")
vk1d & u:: Send("{Blind}{PgDn}")
vk1d & i:: Send("{Blind}{PgUp}")
vk1d & o:: Send("{Blind}{End}")
vk1d:: Send("{Blind}{vk1d}")

;; switch tabs with the mouse wheel for Firefox
#HotIf K5_mouseCursorIsInTheTabBarOfFirefox()
$WheelDown:: {
    global K5_g_mousePosWinId
    ControlSend("{CtrlDown}{PgDn}{CtrlUp}",
              , Format("ahk_id {1:d}", K5_g_mousePosWinId))
}
$WheelUp:: {
    global K5_g_mousePosWinId
    ControlSend("{CtrlDown}{PgUp}{CtrlUp}",
              , Format("ahk_id {1:d}", K5_g_mousePosWinId))
}
#HotIf

;; switch tabs with the mouse wheel for Thunderbird
#HotIf K5_mouseCursorIsInTheTabBarOfThunderbird()
$WheelDown:: {
    global K5_g_mousePosWinId
    ControlSend("{CtrlDown}{PgDn}{CtrlUp}",
              , Format("ahk_id {1:d}", K5_g_mousePosWinId))
}
$WheelUp:: {
    global K5_g_mousePosWinId
    ControlSend("{CtrlDown}{PgUp}{CtrlUp}",
              , Format("ahk_id {1:d}", K5_g_mousePosWinId))
}
#HotIf
